import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import { Home } from './pages/home/Home'
import { Movie } from './pages/movie/Movie'
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbars from './components/navbar/Navbar'
import {GetPopularMovie} from './components/api/Api'
import React from 'react'

function App() {

  function saveStateToLocalStorage(state) {
    try {
      const serializedState = JSON.stringify(state);
      localStorage.setItem('state', serializedState);
    } catch (error) {
      console.error('Error saving state to local storage:', error);
    }
  }

  function loadStateFromLocalStorage() {
    try {
      const serializedState = localStorage.getItem('state');
      if (serializedState === null) {
        return undefined;
      }
      return JSON.parse(serializedState);
    } catch (error) {
      console.error('Error loading state from local storage:', error);
      return undefined;
    }
  }
  const [popular, setPopular] = React.useState(loadStateFromLocalStorage() || [])


  React.useEffect(()=>{
    GetPopularMovie().then((result)=>{
      setPopular(result);
      saveStateToLocalStorage(result);
    });
}, [])
  return (
    <Router>
        <Navbars popular={popular} setPopular={setPopular}/>
      <Routes>
        <Route exact path='/' element=<Home popular={popular} setPopular={setPopular}/> />
        <Route exact path='/movie/:movie_id' element=<Movie popular={popular} setPopular={setPopular}/> />
      </Routes>
    </Router>

  );
}

export default App;
