import React, { useState, useEffect } from 'react';
import Card from '../../components/cards/Card';
import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './home.css';
import YouTube from 'react-youtube';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


export const Home = ({popular}) => {
    const [videoHeight, setVideoHeight] = useState('200');
    const [expanded, setExpanded] = useState(false)
    const dataForDisplay = expanded ? popular : popular.slice(0, 4)

    useEffect(() => {
        const handleResize = () => {
            if (window.innerWidth >= 768) {
                setVideoHeight('768');
            } else {
                setVideoHeight('200');
            }
        };

        window.addEventListener('resize', handleResize);
        handleResize();

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    const opts = {
        width: '100%',
        height: videoHeight,
    };

    const [videos] = useState([
        'qEVUtrk8_B4',
        'd9MyW72ELq0',
        'W3E74j_xFtg',
        // add more video IDs here
    ]);
    


    const settings = {
        dots: true, // show navigation dots
        infinite: true, // enable infinite scrolling
        speed: 500, // transition speed in milliseconds
        slidesToShow: 1, // number of slides to show at once
        slidesToScroll: 1, // number of slides to scroll at a time
        autoplay:true,
        autplayspeed:1000
    };
    

    return (
        <div className='mt-5 container'>
            <div className='line mb-5 mt-5'><h3 className='text-light ms-2'>Recomendation</h3></div>
            <Slider {...settings}>
                 {videos.map((videoId) => (
            <div key={videoId}>
            <YouTube videoId={videoId} opts={opts} className="youtube-video" />
         </div>
    ))}
</Slider>

            <div className='line mb-5 mt-5'><h3 className='text-light ms-2'>Featured</h3></div>
            <Row>
                {dataForDisplay && dataForDisplay.map((movie) => (
                    <Col style={{width:'18rem', margin:'auto'}} md={3} key={movie.id}>
                        <Card
                            title={movie.title}
                            releaseDate={movie.release_date}
                            image={`${process.env.REACT_APP_BASEIMGURL}${movie.poster_path}`}
                            alt={movie.title}
                        />
                        <Button variant="danger" className='mb-5 home--button'><Link to={`/movie/${movie.id}`}> About the movie</Link></Button>
                        
                    </Col>
                  
                ))}
                <Button variant="transparent" className='mb-5 text-light home--button--extnd'  onClick={() => setExpanded(!expanded)}>  {expanded ? 'Show Less' : 'Show More'} </Button>
            </Row>
        </div>
    );
};
