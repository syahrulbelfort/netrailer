import React from 'react';
import { useParams } from 'react-router-dom';
import { Container, Row, Col, Spinner,Button } from 'react-bootstrap';
import { GetOneMovie } from '../../components/api/Api';
import './movie.css'

export const Movie = ({ popular, setPopular }) => {
  const { movie_id } = useParams();
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    const fetchMovie = async () => {
      setIsLoading(true);
      try {
        const result = await GetOneMovie(movie_id);
        setPopular(result);
      } finally {
        setIsLoading(false);
      }
    };
  
    fetchMovie();
  }, [movie_id, setPopular]);
  

  return (
    <Container className='text-light'>
      {isLoading ? (
        <Spinner animation="border" />
      ) : (
        <>
          <h1>{popular.title}</h1>
        <Row>
          <Col>
          <img alt={popular.title} className='mt-3 border' src={`${process.env.REACT_APP_BASEIMGURL}${popular.poster_path}`}/>
          </Col>
          <Col className='mt-5'>
            <h3>Official website : <br/><a rel="noreferrer" className='a--movie' href={popular.homepage} target='_blank'>{popular.homepage}</a></h3>
            <hr/>
            <h3>Original language : <br/><p>{popular.original_language}</p></h3>
            <hr/>
            <h3>Genre : <br/><p>{popular.genres[0].name}</p></h3>
            <hr/>
            <h3>Overview : <br/><p className='p--movie'>{popular.overview}</p></h3>
            <hr/>
            <h3>Release date : <br/><p className='p--movie'>{popular.release_date}</p></h3>
            <hr/>
            <Button variant="danger"><a href='/'>Back to home</a></Button>
            <hr/>
          </Col>
        </Row>
        </>
      )}
    </Container>
  );
};
