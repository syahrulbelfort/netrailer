import { Col, Row } from 'react-bootstrap';
import './card.css'

function Cards(props) {
  return (
      <Row>
        <Col md={3}>
          <div style={{ width: '18rem', border:'none', paddingBottom:'10px'}} className="mb-2 cards">
            <img alt={props.alt} className='mb-3' style={{ width: '170px',height:"250px", objectFit: "fill"}} variant="top" src={props.image} />
              <p className='text-custom' style={{width:"200px"}}>{props.title}</p>
              <p className='text-light'>{props.releaseDate}</p>
          </div>
        </Col>
      </Row>
  );
}

export default Cards;
