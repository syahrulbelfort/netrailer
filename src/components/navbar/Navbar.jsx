import Container from 'react-bootstrap/Container';
import {Nav,Navbar} from 'react-bootstrap';
import Search from '../search/Search'
import './navbar.css'


function Navbars({popular,setPopular}) {
  return (
    <Navbar bg="dark" variant='dark' className='nav-bg mb-5'   expand="lg">
      <Container className='mt-3'>
        <Navbar.Brand className='brand text-danger' href="#home">Netrailer</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/">Movies</Nav.Link>
            <Nav.Link href="#link">TV Series</Nav.Link>
          </Nav>
            <Search popular={popular} setPopular={setPopular}/>
          </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Navbars;