import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { SearchMovie } from '../api/Api';

export default function Search({ setPopular }) {
  const [searchQuery, setSearchQuery] = useState('');

  const search = async () => {
    if (searchQuery.length > 3) {
      const query = await SearchMovie(searchQuery);
      setPopular(query.results);
    }
  };

  return (
    <div>
      <Form className="d-flex">
        <Form.Control
          type="search"
          placeholder="Search"
          className="me-2 ps-5"
          aria-label="Search"
          value={searchQuery}
          onChange={({ target }) => setSearchQuery(target.value)}
        />
        <Button variant="outline-danger" onClick={search}>
          Search
        </Button>
      </Form>
    </div>
  );
}
