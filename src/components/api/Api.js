import axios from 'axios';

const baseUrl = process.env.REACT_APP_BASEURL
const apiKey = process.env.REACT_APP_APIKEY

const popularMovie = `${baseUrl}/movie/popular?page=1&api_key=${apiKey}`;

export const GetPopularMovie = async () => {
const movie = await axios.get(popularMovie)
return movie.data.results
};

export const SearchMovie = async (q) =>{
  const search = await axios.get(`${baseUrl}/search/movie?page=1&query=${q}&api_key=${apiKey}`)
  return search.data
}

export const GetOneMovie = async (movie_id)=>{
  const getOne = await axios.get(`${baseUrl}/movie/${movie_id}?page=1&api_key=${apiKey}`)
  return getOne.data
}

export const GetVideos = async (movie_id)=>{
  const getOnveVideo = await axios.get(`${baseUrl}/movie/${movie_id}/videos?&api_key=${apiKey}`)
  return getOnveVideo.data.results
}
